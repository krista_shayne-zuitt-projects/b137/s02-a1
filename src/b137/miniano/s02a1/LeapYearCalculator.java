package b137.miniano.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {
    public static void main(String[] args) {
        System.out.println("Leap Year Calculator\n");

        //Scanner appScanner = new Scanner(System.in);

        //System.out.println("What is your first name?\n");
        //String firstName = appScanner.nextLine();
        //System.out.println("Hello, " + firstName + "!\n");

        //Activity: Create a program that check if a year is a leap year or not

        int year;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter any Year:");
        year = scan.nextInt();
        scan.close();

        if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
            System.out.println(year + " is a Leap Year.");
        }
        else {
            System.out.println(year + " is not a Leap Year.");
        }

        }

}
